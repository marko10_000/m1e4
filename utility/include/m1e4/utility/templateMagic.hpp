// Lizenziert unter der EUPL
// SPDX-License-Identifier: EUPL-1.2
#pragma once
#include <string_view>
#include <type_traits>
#include <utility>

namespace m1e4::utility {
	//
	// Gruppendokumentation
	//
	/**
	 * \~English
	 * @defgroup TemplateMagic Template Magic
	 * @ingroup Utility
	 *
	 * \~German
	 * @defgroup TemplateMagic Template-Magie
	 * @ingroup Utility
	 */
	// TODO: Beschreibung

	//
	// Basistypen
	//
	/**
	 * @ingroup TemplateMagic
	 * \~English
	 * @brief Contains a single charactor.
	 * @tparam CHAR Character to store.
	 *
	 * \~German
	 * @brief Beinhaltet ein Zeichen.
	 * @tparam CHAR Zeichen zum speichern.
	 */
	template<char CHAR>
	struct TChar final {
		public:
			/// \~English The stored character.
			/// \~German Das gespeicherte Zeichen.
			static constexpr char value = CHAR;
	};
	/**
	 * @ingroup TemplateMagic
	 * \~English
	 * @brief Contains a singel integer.
	 * @tparam INTEGER Integer to store.
	 *
	 * \~German
	 * @brief Beinhaltet eine Zahl.
	 * @tparam INTEGER Zahl die gespeichert werden soll.
	 */
	template<long INTEGER>
	struct TInteger final {
		public:
			/// \~English The stored integer.
			/// \~German Die gespeicherte Zahl.
			static constexpr long value = INTEGER;
	};
	/**
	 * @ingroup TemplateMagic
	 * \~English
	 * @brief Stores list of types.
	 * @tparam CLASSES types to be stored.
	 *
	 * \~German
	 * @brief Speichert eine liste an Typen.
	 * @tparam CLASSES Typen zum Speichern.
	 */
	template<class... CLASSES>
	struct TTypes final {
		private:
			/**
			 * \~English
			 * @brief Iterator over the list.
			 * @tparam TS First declaration with the special case of an empty list.
			 *
			 * \~German
			 * @brief Iterator über eine Liste.
			 * @tparam TS Erste Deklaration mit dem Spezialfall leere list.
			 */
			template<class... TS>
			struct _Iterator final {
				public:
					/**
					 * \~English
					 * @brief Checks if a type is contain in the list. Is always false because special case of empty list.
					 * @tparam C Type to search for.
					 *
					 * \~German
					 * @brief Überprüft ob ein Typ in der Liste vorhanden ist. Ist immer falsch, da es der
					 * Leere-Listen-Spezialfall ist.
					 * @tparam C Typ, nachdem gesucht werden sollte.
					 */
					template<class C>
					static constexpr bool contains = false;
					/// \~English Is true when the current iteration is unique. Is always true, because empty lists content is
					/// unique.
					/// \~German Wahr, wenn die momentane Iteration keine Duplikate besitzt. Ist immer wahr, da des eine leere
					/// Liste ist.
					static constexpr bool isUnique = true;
			};
			/**
			 * \~English
			 * @brief Iterator over the list.
			 * @tparam CURRENT Current type to process.
			 * @tparam NEXT Next types to process.
			 *
			 * \~English
			 * @brief Iterator über eine Liste.
			 * @tparam CURRENT Momentaner Typ, der verarbeitet werden soll.
			 * @tparam NEXT Nächste Typen, die verarbeitet werden sollen.
			 */
			template<class CURRENT, class... NEXT>
			struct _Iterator<CURRENT, NEXT...> final {
				public:
					/**
					 * \~English
					 * @brief Checks if a type is contain in the list.
					 * @tparam C Type to search for.
					 *
					 * \~German
					 * @brief Überprüft ob ein Typ in der Liste vorhanden ist.
					 * @tparam C Typ, nachdem gesucht werden sollte.
					 */
					template<class C>
					static constexpr bool contains = ::std::is_same_v<CURRENT, C> || _Iterator<NEXT...>::template contains<C>;
					/// \~English Is true when the current iteration is unique.
					/// \~German Wahr, wenn die momentane Iteration keine Duplikate besitzt.
					static constexpr bool isUnique
						= (!_Iterator<NEXT...>::template contains<CURRENT>) &&_Iterator<NEXT...>::isUnique;
			};

			/**
			 * \~English
			 * @brief Helper type to get type by index. POS != 0 case.
			 * @tparam POS Current position of the index access.
			 * @tparam CURRENT Current type to process.
			 * @tparam NEXT Future types to process.
			 *
			 * \~German
			 * @brief Hilfstyp um einen Typ an einem gewissen Index zu bekommen. POS != 0 Fall.
			 * @tparam POS Momentane Position von dem Indexzugriff.
			 * @tparam CURRENT Momentaner Typ zu verarbeiten.
			 * @tparam NEXT Zukünftige Typen zu verarbeiten.
			 */
			template<size_t POS, class CURRENT, class... NEXT>
			struct _TypeGet final {
				public:
					/// \~English Result type.
					/// \~German Ergebnistyp.
					using type = typename _TypeGet<POS - 1, NEXT...>::type;
			};
			/**
			 * \~English
			 * @brief Helper type to get type by index. Special case for corrent type.
			 * @tparam CURRENT Current type to process.
			 * @tparam NEXT Future types to process.
			 *
			 * \~German
			 * @brief Hilfstyp um einen Typ an einem gewissen Index zu bekommen. Ergebnis ist momentaner Typ.
			 * @tparam CURRENT Momentaner Typ zu verarbeiten.
			 * @tparam NEXT Zukünftige Typen zu verarbeiten.
			 */
			template<class CURRENT, class... REST>
			struct _TypeGet<0, CURRENT, REST...> final {
				public:
					/// \~English Result type.
					/// \~German Ergebnistyp.
					using type = CURRENT;
			};

		public:
			/// \~English Is true when a type isn't listed twice else false.
			/// \~German Ist wahr, wenn kein Typ zweimal vorkommen, ansonsten falsch.
			static constexpr bool isUnique = _Iterator<CLASSES...>::isUnique;

			/**
			 * \~English
			 * @brief Is true when the give type is contained in the list else false.
			 * @tparam TYPE Type to search for.
			 *
			 * \~German
			 * @brief Ist wahr, wenn der übergebene Typ in der Liste ist, ansonsten ist es falsch.
			 * @tparam TYPE Typ, nachdem gesucht werden soll.
			 */
			template<class TYPE>
			static constexpr bool contains = _Iterator<CLASSES...>::template contains<TYPE>;

			/**
			 * \~English
			 * @brief Get a type on a specific position.
			 * @tparam POS Position inside of the content.
			 *
			 * \~German
			 * @brief Bestimmt den Typ an einen spezifischen Position.
			 * @tparam POS Position im Container.
			 */
			template<size_t POS>
			requires(POS < sizeof...(CLASSES)) using getType = _TypeGet<POS, CLASSES...>::type;
	};
	/**
	 * @ingroup TemplateMagic
	 * \~English
	 * @brief Container for a template interger (longs) array.
	 * @tparam INTEGERS Integers to store.
	 *
	 * \~German
	 * @brief Container für eine Liste von Ganzzahlen (Longs).
	 * @tparam INTEGERS Ganzzahlen zum speichern.
	 */
	template<long... INTEGERS>
	struct TIntegers final {
		private:
			/**
			 * \~English
			 * @brief Checks if the integer list is unique.
			 * @return True when it's unique else false.
			 *
			 * \~German
			 * @brief Überprüft, ob die Zahlenliste keine Duplikate besitzt.
			 * @return Wahr, wenn keine Duplikate gefunden wurden, ansonsten falsch.
			 */
			static constexpr bool _isUnique() {
				if constexpr(sizeof...(INTEGERS) == 0) {
					return true;
				}
				else {
					long content[sizeof...(INTEGERS)] = {INTEGERS...};
					for(decltype(sizeof...(INTEGERS)) i = 0; i < sizeof...(INTEGERS); i++) {
						for(decltype(sizeof...(INTEGERS)) j = i + 1; j < sizeof...(INTEGERS); j++) {
							if(content[i] == content[j]) {
								return false;
							}
						}
					}
					return true;
				}
			};

			/**
			 * \~English
			 * @brief Check if a value is contained in the integer list.
			 * @param value Value to search.
			 * @return True when the value is contained else false.
			 *
			 * \~German
			 * @brief Überprüft, ob ein Wert in der Zahlenliste vorhanden ist.
			 * @param value Wert, der gesucht werden soll.
			 * @return Wahr, wenn der Wert beinhaltet ist, ansonsten falsch.
			 */
			static constexpr bool _contains(long value) {
				if constexpr(sizeof...(INTEGERS) == 0) {
					return false;
				}
				else {
					long content[sizeof...(INTEGERS)] = {INTEGERS...};
					for(decltype(sizeof...(INTEGERS)) i = 0; i < sizeof...(INTEGERS); i++) {
						if(content[i] == value) {
							return true;
						}
					}
					return false;
				}
			}

		public:
			/**
			 * \~English
			 * @brief Checks if a value is contained.
			 * @tparam VALUE Value to be searched.
			 *
			 * \~German
			 * @brief Überprüft, ob ein Wert in der Liste beinhaltet ist.
			 * @tparam VALUE Wert, nach dem gesucht werden soll.
			 */
			template<long VALUE>
			static constexpr bool contains = _contains(VALUE);

			/// \~English Is true when no number appears twice else false.
			/// \~German Ist wahr, wenn keine Zahl zweimal auftaucht, ansonsten falsch.
			static constexpr bool isUnique = _isUnique();

			/// \~English Is a TTypes list with TInteger-s.
			/// \~German Ist eine TTypes liste mit TInteger-s.
			using asTTypes = ::m1e4::utility::TTypes<::m1e4::utility::TInteger<INTEGERS>...>;
	};
	/**
	 * @ingroup TemplateMagic
	 * \~English
	 * @brief Container einen compiletime string.
	 * @tparam STRING String to contain.
	 *
	 * \~German
	 * @brief Beinhaltet eine Zeichenkette, die während der Übersetzung benutzt verarbeitet werden kann.
	 * @tparam STRING Zeichenkette, die gespeichert werden soll.
	 */
	template<char... STRING>
	struct TString final {
		private:
			/// \~English Helper value to store raw content.
			/// \~German Hilfsvariable um die rohe Zeichenkette zwischenzuspeichern.
			static constexpr char contentRaw[sizeof...(STRING)] = {STRING...};

		public:
			/// \~English Length of the string.
			/// \~German Länge der Zeichenkette.
			static constexpr auto size = sizeof...(STRING);
			/// \~English The string it self.
			/// \~German Die Zeichenkette an sich.
			static constexpr ::std::string_view content = ::std::string_view(contentRaw, size);
	};
	/// @private
	template<>
	struct TString<> final {
		public:
			/// \~English Length of the string.
			/// \~German Länge der Zeichenkette.
			static constexpr auto size = 0;
			/// \~English The string it self.
			/// \~German Die Zeichenkette an sich.
			static constexpr ::std::string_view content = ::std::string_view("", 0);
	};

	//
	// Helfer
	//
	/// @cond IGNORE_DOXYGEN
	namespace internal {
		/**
		 * \~English
		 * @brief Enumerates integers. TO < FROM case.
		 * @tparam FROM Source number to count from.
		 * @tparam TO Target number to count.
		 * @tparam VALUES List of values to insert.
		 *
		 * \~German
		 * @brief Zählt Zahlen auf. TO < FROM Fall.
		 * @tparam FROM Anfangszahl.
		 * @tparam TO Zielzahl.
		 * @tparam VALUES Bereits aufgezahlte Zahlen.
		 */
		template<long FROM, long TO, long... VALUES>
		struct TIntegersFT final {
			public:
				/// \~English Enumerated content.
				/// \~German Aufgezählte Inhalt.
				using type = ::m1e4::utility::TIntegers<VALUES...>;
		};
		/**
		 * \~English
		 * @brief Enumerates integers. FROM <= TO case.
		 * @tparam FROM Source number to count from.
		 * @tparam TO Target number to count.
		 * @tparam VALUES List of values to insert.
		 *
		 * \~German
		 * @brief Zählt Zahlen auf. FROM <= TO Fall.
		 * @tparam FROM Anfangszahl.
		 * @tparam TO Zielzahl.
		 * @tparam VALUES Bereits aufgezahlte Zahlen.
		 */
		template<long FROM, long TO, long... VALUES>
		requires(FROM <= TO) struct TIntegersFT<FROM, TO, VALUES...> {
			public:
				/// \~English Enumerated content.
				/// \~German Aufgezählte Inhalt.
				using type = typename ::m1e4::utility::internal::TIntegersFT<FROM + 1, TO, VALUES..., FROM>::type;
		};

		/**
		 * \~English
		 * @brief TString generator.
		 * @tparam GENERATOR Generator function to use to get values.
		 * @tparam SIZE Size of the TString.
		 *
		 * \~German
		 * @brief TString-Generator.
		 * @tparam GENERATOR Generator-Funktion um die Zeichen auszulesen.
		 * @tparam SIZE Größe des TString Zeichenkette.
		 */
		template<class GENERATOR, long SIZE>
		struct TStringGenerator {
			private:
				/**
				 * \~English
				 * @brief Helper class to index TString entires.
				 * @tparam T Indexes container.
				 *
				 * \~German
				 * @brief Hilfsklasse um die Indexes des TString Einträge.
				 * @tparam T Indexes-Behälter.
				 */
				template<class T>
				struct _Helper;
				/**
				 * \~English
				 * @brief Helper class to index TString entires.
				 * @tparam INDEXES Indexes of the TString.
				 *
				 * \~German
				 * @brief Hilfsklasse um die Indexes des TString Einträge.
				 * @tparam INDEXES Indexes von dem TString.
				 */
				template<long... INDEXES>
				struct _Helper<::m1e4::utility::TIntegers<INDEXES...>> {
					public:
						/// \~English Generate TString from generator.
						/// \~German Generiere TString vom Generator.
						using type = ::m1e4::utility::TString<decltype(::std::declval<GENERATOR>()(
							::m1e4::utility::TInteger<INDEXES> {}))::value...>;
				};

			public:
				/// \~English Generated string.
				/// \~German Generierte Zeichenkette.
				using type = typename _Helper<typename ::m1e4::utility::internal::TIntegersFT<0, SIZE - 1>::type>::type;
		};

		/**
		 * \~English
		 * @brief Basic processing of TTypes. Not a TTypes type case.
		 * @tparam TYPE Type to analyse.
		 *
		 * \~German
		 * @brief Simple Verarbeitung von TTypes. Nicht TTypes Fall.
		 * @tparam TYPE Typ zu analysieren.
		 */
		template<class TYPE>
		struct TTypesBasic final {
			public:
				/// \~English Isn't a TTypes.
				/// \~German Ist kein TTypes.
				static constexpr bool isTTypes = false;
		};
		/**
		 * \~English
		 * @brief Basic processing of TTypes. Not a TTypes type case.
		 * @tparam TYPES TTypes content.
		 *
		 * \~German
		 * @brief Simple Verarbeitung von TTypes. Nicht TTypes Fall.
		 * @tparam TYPES TTypes Inhalt.
		 */
		template<class... TYPES>
		struct TTypesBasic<::m1e4::utility::TTypes<TYPES...>> {
			public:
				/// \~English Is a TTypes.
				/// \~German Ist ein TTypes.
				static constexpr bool isTTypes = true;
		};
	} // namespace internal
	/// @endcond

	/**
	 * @ingroup TemplateMagic
	 * \~English
	 * @brief Check if a type is a TTypes.
	 * @tparam TYPE Type to check.
	 *
	 * \~German
	 * @brief Überprüfe ob der Typ ist TTypes.
	 * @tparam TYPE Typ zu überprüfen.
	 */
	template<class TYPE>
	static constexpr bool isTTypes = ::m1e4::utility::internal::TTypesBasic<TYPE>::isTTypes;
	/**
	 * @ingroup TemplateMagic
	 * \~English
	 * @brief Concept to enforce a TTypes type.
	 * @tparam TYPE Type to check.
	 *
	 * \~German
	 * @brief Konzept um ein TTypes-Typ zu erzwingen.
	 * @tparam TYPE Typ zu überprüfen.
	 */
	template<class TYPE>
	concept TTypesConcept = (::m1e4::utility::isTTypes<TYPE>);

	/**
	 * @ingroup TemplateMagic
	 * \~English
	 * @brief Concept to enforce that the content of the TTypes is unique.
	 * @tparam TYPE Type to check.
	 *
	 * \~German
	 * @brief Konzept zum Erzwingen, dass der Inhalt von TTypes keine Duplikate hat.
	 * @tparam TYPE Typ zu überprüfen.
	 */
	template<class TYPE>
	concept UniqueTTypes = ::m1e4::utility::TTypesConcept<TYPE> &&(TYPE::isUnique);
	/**
	 * @ingroup TemplateMagic
	 * \~English
	 * @brief Concept to enforce that a type is contained in a TTypes.
	 * @tparam TYPE Type to check to be contained.
	 * @tparam TTYPES Type list to search in.
	 *
	 * \~German
	 * @brief Konzept zum erzwingen, dass ein Typ in einem TTypes beinhaltet ist.
	 * @tparam TYPE Typ der beinhaltet sein soll.
	 * @tparam TTYPES Typliste in der gesucht werden soll.
	 */
	template<class TYPE, class TTYPES>
	concept ContainedInTTypes = ::m1e4::utility::TTypesConcept<TTYPES> &&(TTYPES::template contains<TYPE>);

	/**
	 * @ingroup TemplateMagic
	 * \~English
	 * @brief Generates an integer range specified by the parameters `FROM` to `TO`. When `TO` < `FROM` then the range will be
	 * empty.
	 * @tparam FROM Starting point of the range.
	 * @tparam TO End point of the range. Number it self will be included iff `TO` >= `FROM`.
	 *
	 * \~German
	 * @brief Generiert einen Zahlenbereich, der, mithilfe der Templateparameter, sich von `FROM` bis `TO` erstreckt. Sollte `TO`
	 * < `FROM` sein, so wird der Bereich leer sein.
	 * @tparam FROM Startpunkt von dem Zahlenbereich.
	 * @tparam TO Endpunkt des Zahlenbereich. Das Ende ist dabei inklusive, solange `TO` >= `FROM`.
	 */
	template<long FROM, long TO>
	using TIntegersBoundary = typename ::m1e4::utility::internal::TIntegersFT<FROM, TO>::type;
	/**
	 * @ingroup TemplateMagic
	 * \~English
	 * @brief Generates an interger range from 0 to `SIZE`. `SIZE` it self is excluded.
	 * @tparam SIZE Size of the range.
	 *
	 * \~German
	 * @brief Generiert einen Zahlenbereich von 0 bis `SIZE`. `SIZE` an sich ist dabei ausgenommen.
	 * @tparam SIZE Größe des Zahlenbereichs.
	 */
	template<long SIZE>
	using TIntegersFromSize = typename ::m1e4::utility::TIntegersBoundary<0, SIZE - 1>;
} // namespace m1e4::utility

/**
 * @ingroup TemplateMagic
 * \~English
 * @brief Generates a template string type.
 * @param STRING String to insert into the template string.
 *
 * \~German
 * @brief Geniert ein Templatezeichenkettetyp.
 * @param STRING Zeichenkette, die in ein Templatezeichenkettetyp umgewandelt werden soll.
 */
#define TSTRING_T(STRING)                                                                        \
	decltype([]() -> auto{                                                                       \
		static constexpr const char v[] = STRING;                                                \
		static constexpr auto f         = [](auto pos) -> auto{                                  \
					return ::m1e4::utility::TChar<v[decltype(pos)::value]> {};                   \
		};                                                                                       \
		return ::m1e4::utility::internal::TStringGenerator<decltype(f), sizeof(v) - 1>::type {}; \
	}())
/**
 * @ingroup TemplateMagic
 * \~English
 * @brief Generates a template string object.
 * @param STRING String to instert into the template string object.
 *
 * \~German
 * @brief Generiert eine Templatezeichenkette.
 * @param STRING Zeichenkette, die in der Templatezeichkette beinhaltet ist.
 */
#define TSTRING(STRING) (TSTRING_T(STRING) {})

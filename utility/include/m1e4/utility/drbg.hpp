// Lizenziert unter der EUPL
// SPDX-License-Identifier: EUPL-1.2
#pragma once
#include <concepts>
#include <cstdint>

namespace m1e4::utility {
	//
	// Gruppendokumentation
	//
	/**
	 * \~English
	 * @defgroup DRBG Deterministic random bit generator
	 * @ingroup Utility
	 *
	 * \~German
	 * @defgroup DRBG Deterministische Zufallszahlengenerator
	 * @ingroup Utility
	 */
	// TODO: Beschreibung

	//
	// Basiskonzept
	//
	/**
	 * @ingroup DRBG
	 * \~English
	 * @brief Concept to check a deterministic random generator.
	 * @tparam TYPE The type to check.
	 *
	 * \~German
	 * @brief Konzept zum Überprüfen, dass es ein deterministische Zufallsgenerator ist.
	 * @tparam TYPE Typ zum überprüfen.
	 */
	// TODO: Beschreibung
	template<class TYPE>
	concept DRBG = requires(TYPE type, const TYPE constType) {
		{ constType } -> ::std::convertible_to<bool>;
		{ constType.currentValue() } -> ::std::same_as<uint64_t>;
		{ constType.currentValue(static_cast<uint64_t>(0)) } -> ::std::same_as<uint64_t>;
		{ type.heavyShuffle() } -> ::std::same_as<void>;
		{ type.mixIn(static_cast<uint64_t>(0)) } -> ::std::same_as<void>;
		{ type.mixIn(constType) } -> ::std::same_as<void>;
		{ type.next() } -> ::std::same_as<void>;
		{ constType.state } -> ::std::convertible_to<const uint64_t*>;
	}
	&&(!::std::same_as<decltype(::std::declval<const TYPE>().state), const uint64_t*>) &&::std::semiregular<TYPE>;

	//
	// Varianten
	//
	/**
	 * @ingroup DRBG
	 * \~English
	 * @brief Xorshift+ 128-bit variant.
	 * @details To use it correctly it has to be initalized.
	 * To do that use the `mixIn` function to add base randomness by the parameter input.
	 * It has to be called at least ones.
	 * After that a `heavyShuffle` should be executed and than the generator can be used.
	 * @warning Has to be initalized or will output 0 every time.
	 * @warning Not suitable for cryptographic random bits.
	 *
	 * \~German
	 * @brief Xorshift+ 128-Bit-Variante.
	 * @details Um den Generator richtig zu verwenden, muss dieser richtig in­i­ti­a­li­sie­rt werden.
	 * Um dies zu erreichen sollte zuerst der Generator mit Zufallswerte, mittels der `mixIn` Funktion, gefüttert werden.
	 * Dies muss mindestens einmal gemacht werden.
	 * Dannach sollte einmal `heavyShuffle` aufgerufen werden, damit jedes Bit sich auf jedes andere Bit auswirken kann.
	 * Damit steht dann der Generator bereit zur Benutzung.
	 * @warning Muss initialisiert werden oder es wird immer 0 ausgeben.
	 * @warning Ist nicht für kryptografische Zufallsbits geeignet.
	 */
	struct XorshiftPlus128 final {
		public:
			/// \~English State of the generator.
			/// \~German Status des Generators.
			uint64_t state[2] = {0, 0};

			/**
			 * \~English
			 * @brief Construct a new XorshiftPlus 128 bit object.
			 *
			 * \~German
			 * @brief Standard-Konstruktor von einem neuem XorshiftPlus 128-Bit Objekt.
			 */
			XorshiftPlus128() {}
			/**
			 * \~English
			 * @brief Construct a new XorshiftPlus 128 bit object by state.
			 * @param state State to insert.
			 * @note The generator is always initalized correctly after that.
			 *
			 * \~German
			 * @brief Konstruktor von einem neue XorshiftPlus 128-Bit Objekt mit übergebenem Zustand.
			 * @param state Status des Generators.
			 * @note Der Generator ist dannach immer korrekt in­i­ti­a­li­sie­rt.
			 */
			XorshiftPlus128(uint64_t state[2]) {
				this->state[0] = state[0];
				this->state[1] = state[1];
				if(!*this) {
					this->state[0] = 1;
				}
			}

			/**
			 * \~English
			 * @brief To check if generator is initialized.
			 * @return True when correctly initialized else false.
			 *
			 * \~German
			 * @brief Zum überprüfen, ob der Generator initialisiert ist.
			 * @return Wahr, wenn der Generator initialisiert ist, ansonsten falsch.
			 */
			operator bool() const {
				return (this->state[0] != 0) || (this->state[1] != 0);
			}

			uint64_t currentValue() const {
				return this->state[0] + this->state[1];
			}
			uint64_t currentValue(uint64_t mod) const {
				uint64_t oddMod = mod | 1;
				return (this->currentValue() % oddMod) % mod;
			}
			/**
			 * \~English
			 * @brief Shuffles
			 */
			void heavyShuffle() {
				for(int i = 0; i < 50; i++) { // 25 Iterationen
					this->next();
				}
			}
			/**
			 * \~English
			 * @brief Mix in 64 bit into the generator.
			 * @param value Value to mix in.
			 * @note After this function call the generator is always initialized.
			 *
			 * \~German
			 * @brief Füge 64 Bit in den Generator ein.
			 * @param value Wert zum einpflegen.
			 * @note Nach dem Funktionsaufruf ist der Generator immer initialisiert.
			 */
			void mixIn(uint64_t value) {
				uint64_t old1  = this->state[1];
				this->state[1] = this->state[0] ^ ((value << 23) + (((~value) ^ this->state[1]) >> 18));
				this->state[1] |= 1;
				this->state[0] = old1;
				this->next();
			}
			/**
			 * \~English
			 * @brief Insert the state of an other xorshift plus 128 bit generator.
			 * @param generator Generator to insert.
			 * @note After this function call the generator is always initialized.
			 *
			 * \~German
			 * @brief Fügt den Zustand von einem anderen Xorshift-Plus 128-Bit-Generator.
			 * @param generator Generator zum einfügen.
			 * @note Nach dem Funktionsaufruf ist der Generator immer initialisiert.
			 */
			void mixIn(XorshiftPlus128 generator) {
				this->mixIn(generator.state[0]);
				this->mixIn(generator.state[1]);
			}
			/**
			 * \~English
			 * @brief Go to the next state.
			 *
			 * \~German
			 * @brief Gehe in den nächsten Zustand über.
			 */
			void next() {
				uint64_t processed = this->state[0];
				this->state[0]     = this->state[1];
				processed ^= processed << 23;
				processed ^= processed >> 18;
				processed ^= this->state[0] ^ (this->state[0] >> 5);
				this->state[1] = processed;
			}
	};
}; // namespace m1e4::utility

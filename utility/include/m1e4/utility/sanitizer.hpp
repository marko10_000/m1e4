#pragma once
//
// Gruppendokumentation
//
/**
 * \~English
 * @defgroup Sanitizer Sanitizer
 * @ingroup Utility
 *
 * \~German
 * @defgroup Sanitizer Überprüfer
 * @ingroup Utility
 */

//
// Memory sanitizer
//
/// @cond
#ifdef VALGRIND_MAKE_MEM_DEFINED
	#define _M1E4_UTILITY_VALGRIND_MEM_DEFINED(MEM, SIZE) VALGRIND_MAKE_MEM_DEFINED(MEM, SIZE)
#else
	#define _M1E4_UTILITY_VALGRIND_MEM_DEFINED(MEM, SIZE)
#endif
#ifdef VALGRIND_MAKE_MEM_UNDEFINED
	#define _M1E4_UTILITY_VALGRIND_MEM_UNDEFINED(MEM, SIZE) VALGRIND_MAKE_MEM_UNDEFINED(MEM, SIZE)
#else
	#define _M1E4_UTILITY_VALGRIND_MEM_UNDEFINED(MEM, SIZE)
#endif
/// @endcond

/**
 * @file
 * @ingroup Sanitizer
 * \~English
 * @brief Add optional memory checking, set it as defined.
 * @param MEM Memory location to set defined.
 * @param SIZE Size of the memory location.
 *
 * \~German
 * @brief Füge optionalen Speicherzugriffüberprüfung hinzu, setzte Bereich als definiert.
 * @param MEM Speicherbereich, der als definiert gelt soll.
 * @param SIZE Größe des Speicherbereichs.
 */
#define M1E4_MEM_MAKE_DEFINED(MEM, SIZE) \
	{ _M1E4_UTILITY_VALGRIND_MEM_DEFINED(MEM, SIZE); }
/**
 * @file
 * @ingroup Sanitizer
 * \~English
 * @brief Add optional memory checking, set it as undefined.
 * @param MEM Memory location to set undefined.
 * @param SIZE Size of the memory location.
 *
 * \~German
 * @brief Füge optionalen Speicherzugriffüberprüfung hinzu, setzte Bereich als undefiniert.
 * @param MEM Speicherbereich, der als undefiniert gelt soll.
 * @param SIZE Größe des Speicherbereichs.
 */
#define M1E4_MEM_MAKE_UNDEFINED(MEM, SIZE) \
	{ _M1E4_UTILITY_VALGRIND_MEM_UNDEFINED(MEM, SIZE); }

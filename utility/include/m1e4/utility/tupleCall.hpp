// Lizenziert unter der EUPL
// SPDX-License-Identifier: EUPL-1.2
#pragma once
#include <m1e4/utility/templateMagic.hpp>
#include <tuple>
#include <type_traits>
#include <utility>

namespace m1e4::utility {
	//
	// Dokumentation
	//
	/**
	 * \~English
	 * @defgroup TupleCall Tuple functioncalls
	 * @ingroup Utility
	 *
	 * \~German
	 * @defgroup TupleCall Tupelfunktionsaufrufe
	 * @ingroup Utility
	 */
	// TODO: Dokumentation

	//
	// Tuplefunktionen
	//
	/// @cond IGNORE_DOXYGEN
	namespace internal {
		/**
		 * \~English
		 * @brief Process tuple type and tuple call. Not a tuple type case.
		 * @tparam TUPLE Type to process as potential tuple.
		 *
		 * \~German
		 * @brief Verarbeite Tupeltyp und Tupelaufrufe. Nicht ein Tupeltyp-Fall.
		 * @tparam TUPLE Typ, der als Tupel verarbeitet werden soll.
		 */
		template<class TUPLE>
		struct TupleWorker final {
			public:
				/// \~English Notify that it isn't a tuple.
				/// \~German Veststellung, dass es kein Tupel ist.
				static constexpr bool isTuple = false;
		};
		/**
		 * \~English
		 * @brief Process tuple type and tuple call. Tuple type case.
		 * @tparam TUPLE Types inside of the tuple.
		 *
		 * \~German
		 * @brief Verarbeite Tupeltyp und Tupelaufrufe. Tupel-Fall.
		 * @tparam TUPLE Typen im Tupel.
		 */
		template<class... TUPLE>
		struct TupleWorker<::std::tuple<TUPLE...>> final {
			public:
				/// \~English Tuple type redefinition.
				/// \~German Tupeltyp erneut definiert.
				using T = ::std::tuple<TUPLE...>;

				/// \~English Notify that it is as tuple.
				/// \~German Veststellung, dass es ein Tupel ist.
				static constexpr bool isTuple = true;
				/**
				 * \~English
				 * @brief Checks if the function is invocable with the tuple itself.
				 * @tparam FUNC Type of the function that should be called.
				 *
				 * \~German
				 * @brief Überprüft, ob eine Funktion mit dem Tupel selbst aufgerufen werden kann.
				 * @tparam FUNC Funktionstyp, der aufgerufen werden soll.
				 */
				template<class FUNC>
				static constexpr bool isInvocableTuple = ::std::is_invocable_v<FUNC, T>;
				/**
				 * \~English
				 * @brief Checks if the function is invocable with the tuple unpacked.
				 * @tparam FUNC Type of the function that should be called.
				 *
				 * \~German
				 * @brief Überprüft, ob eine Funktion mit dem Tupel ausgepackt aufgerufen werden kann.
				 * @tparam FUNC Funktionstyp, der aufgerufen werden soll.
				 */
				template<class FUNC>
				static constexpr bool isFuncCallalabe = ::std::is_invocable_v<FUNC, TUPLE...>;
				/**
				 * \~English
				 * @brief Get the function return type of the invoked function with the tuple itself.
				 * @tparam FUNC Type of the function that should be called.
				 *
				 * \~German
				 * @brief Gibt den Rückgabetyp der Funktion, die mit Tupel selbst aufgerufen wurde.
				 * @tparam FUNC Funktionstyp, der aufgerufen werden soll.
				 */
				template<class FUNC>
				using SimpleCallFunc_t = ::std::invoke_result_t<FUNC, T>;
				/**
				 * \~English
				 * @brief Get the function return type of the invoked function with the tuple unpacked.
				 * @tparam FUNC Type of the function that should be called.
				 *
				 * \~German
				 * @brief Gibt den Rückgabetyp der Funktion, die mit Tupel entpackt aufgerufen wurde.
				 * @tparam FUNC Funktionstyp, der aufgerufen werden soll.
				 */
				template<class FUNC>
				using CallFunc_t = ::std::invoke_result_t<FUNC, TUPLE...>;

			private:
				/**
				 * \~English
				 * @brief Helper type to get the call return type. Tuple direct call variant.
				 * @tparam IS_SIMPLE Variant of function call that should be true.
				 * @tparam FUNC Function to analyse.
				 *
				 * \~German
				 * @brief Hilfstyp, um den Rückgabetyp der Funktion zu erhalten. Variante: Tuple direkt übergeben.
				 * @tparam IS_SIMPLE Variante des Funktionsaufruf, das hier wahr sein sollte.
				 * @tparam FUNC Funktionstyp zum analysieren.
				 */
				template<bool IS_SIMPLE, class FUNC>
				struct _GetResultType final {
					public:
						/// \~English Type of the return value.
						/// \~German Rückgabetyp der Funktion.
						using type = SimpleCallFunc_t<FUNC>;
				};
				/**
				 * \~English
				 * @brief Helper type to get the call return type. Tuple unpack call variant.
				 * @tparam FUNC Function to analyse.
				 *
				 * \~German
				 * @brief Hilfstyp, um den Rückgabetyp der Funktion zu erhalten. Variante: Tuple entpackt übergeben.
				 * @tparam FUNC Funktionstyp zum analysieren.
				 */
				template<class FUNC>
				struct _GetResultType<false, FUNC> final {
					public:
						/// \~English Type of the return value.
						/// \~German Rückgabetyp der Funktion.
						using type = CallFunc_t<FUNC>;
				};

				/// @private
				template<class>
				struct _Caller;
				/**
				 * \~English
				 * @brief Unpacking helper for the function call.
				 * @tparam INDEXES Indexes of the positions in the tuple.
				 *
				 * \~German
				 * @brief Entpackungshilfe für einen Funktionsaufruf.
				 * @tparam INDEXES Indexes der Position im Tupel.
				 */
				template<long... INDEXES>
				struct _Caller<::m1e4::utility::TIntegers<INDEXES...>> final {
					public:
						/**
						 * \~English
						 * @brief Make unpacking function call.
						 * @tparam FUNC Function type that should be called.
						 * @tparam T2 Tuple type to use.
						 * @param func Function objection to call.
						 * @param tuple Tuple that should be processed.
						 * @return Result of the function.
						 *
						 * \~German
						 * @brief Mach einen ausgepackten funktionsaufruf.
						 * @tparam FUNC Funktionstyp, der aufgerufen werden soll.
						 * @tparam T2 Tupletyp der benutzt wurde.
						 * @param func Funktionsobjekt, dass aufgerufen werden soll.
						 * @param tuple Tupel, dass verarbeitet werden soll.
						 * @return Ergebnis der Funktion.
						 */
						template<class FUNC, class T2>
						requires ::std::same_as<::std::remove_cvref_t<T2>, T>
						static ::std::invoke_result_t<FUNC, TUPLE...> callFunc(FUNC&& func, T2&& tuple) {
							return func(::std::forward<typename ::m1e4::utility::TTypes<TUPLE...>::getType<INDEXES>>(
								::std::get<INDEXES>(tuple))...);
						}
				};

			public:
				/**
				 * \~English
				 * @brief Helper to access to the call result.
				 * @tparam FUNC Type of the function that should be analyzed.
				 *
				 * \~German
				 * @brief Werkzeug, um den Rückgabetyp einer Funktion zu erfassen.
				 * @tparam FUNC Funktionstyp zu analysieren.
				 */
				template<class FUNC>
				using getResultType = _GetResultType<isInvocableTuple<FUNC>, FUNC>::type;

				/**
				 * \~English
				 * @brief Makes a function call to call the function with the tuple as is.
				 * @tparam FUNC Function type to call.
				 * @tparam T2 Helper type that have to be the same tuple type.
				 * @return Result of the function.
				 *
				 * \~German
				 * @brief Macht einen Funktionsaufruf mit dem Tupel an sich.
				 * @tparam FUNC Typ von der Funktion.
				 * @tparam T2 Hilfstyp, der identisch mit dem Tupel sein muss.
				 * @return Ergebnis der Funktion.
				 */
				template<class FUNC, class T2>
				requires((isInvocableTuple<FUNC>) &&::std::same_as<::std::remove_cvref_t<T2>,
				                                                   T>) static SimpleCallFunc_t<FUNC> callFunc(FUNC&& func,
				                                                                                              T2&& tuple) {
					return func(::std::forward<T2>(tuple));
				}
				/**
				 * \~English
				 * @brief Makes a function call to call the function with the tuple unpacked, as long as the tuple version isn't a
				 * option.
				 * @tparam FUNC Function type to call.
				 * @tparam T2 Helper type that have to be the same tuple type.
				 * @return Result of the function.
				 *
				 * \~German
				 * @brief Macht einen Funktionsaufruf mit dem Tupel entpack, so lang der Aufruf mit dem Tupel an sich nicht
				 * unterstützt wird.
				 * @tparam FUNC Typ von der Funktion.
				 * @tparam T2 Hilfstyp, der identisch mit dem Tupel sein muss.
				 * @return Ergebnis der Funktion.
				 */
				template<class FUNC, class T2>
				requires((!isInvocableTuple<FUNC>) &&::std::same_as<::std::remove_cvref_t<T2>,
				                                                    T>) static CallFunc_t<FUNC> callFunc(FUNC&& func,
				                                                                                         T2&& tuple) {
					using C = _Caller<::m1e4::utility::TIntegersFromSize<sizeof...(TUPLE)>>;
					return C::callFunc(::std::forward<FUNC>(func), ::std::forward<T2>(tuple));
				}
		};
	} // namespace internal
	/// @endcond

	/**
	 * @ingroup TupleCall
	 * \~English
	 * @brief Checks if type is a tuple.
	 * @tparam TYPE Type to check.
	 *
	 * \~German
	 * @brief Überprüft ob der Typ ein Tupel ist.
	 * @tparam TYPE Typ zu überprüfen.
	 */
	template<class TYPE>
	static constexpr bool isTuple = ::m1e4::utility::internal::TupleWorker<::std::remove_cvref_t<TYPE>>::isTuple;
	/**
	 * @ingroup TupleCall
	 * \~English
	 * @brief Concept to requires that a type is a tuple.
	 * @tparam TYPE Type to check.
	 *
	 * \~German
	 * @brief Konzept, das das ein Tupel erzwingt.
	 * @tparam TYPE Typ zu überprüfen.
	 */
	template<class TYPE>
	concept TupleConcept = (::m1e4::utility::isTuple<TYPE>);
	/**
	 * @ingroup TupleCall
	 * \~English
	 * @brief Check if function is callable by tuple it self or unpacked as r-reference type.
	 * @tparam FUNCTION Function to call and check.
	 * @tparam TUPLE Tuple to insert into the function.
	 *
	 * \~German
	 * @brief Überprüft ob einen Funktion mit einem Tupel an sich oder aufgeteilt aufgerufen werden kann. Die Element(e) werde als
	 * R-Referenz zugewiesen.
	 * @tparam FUNCTION Funktion zu überprüfen.
	 * @tparam TUPLE Tupeltyp, der in die Funktion eingefügt werden.
	 */
	template<class FUNCTION, ::m1e4::utility::TupleConcept TUPLE>
	static constexpr bool isTupleCallable
		= ::m1e4::utility::internal::TupleWorker<::std::remove_cvref_t<TUPLE>>::template isInvocableTuple<
			  FUNCTION> || ::m1e4::utility::internal::TupleWorker<::std::remove_cvref_t<TUPLE>>::template isFuncCallalabe<FUNCTION>;
	/**
	 * @ingroup TupleCall
	 * \~English
	 * @brief Concept of a callable function which is callable with the tuple it self or unpacked as r-reference type.
	 * @tparam FUNCTION Function to check.
	 * @tparam TUPLE Tuple to insert into the function.
	 *
	 * \~German
	 * @brief Konzept zum Erzwingen, das eine Funktion sich mit einem Tupel an sich oder aufgeteilt aufgerufen werden kann. Die
	 * Element(e) werde als R-Referenz zugewiesen.
	 * @tparam FUNCTION Funktion zu überprüfen.
	 * @tparam TUPLE Tupeltyp, der in die Funktion eingefügt werden.
	 */
	template<class FUNCTION, class TUPLE>
	concept TupleCallable = ::m1e4::utility::TupleConcept<TUPLE> &&(::m1e4::utility::isTupleCallable<FUNCTION, TUPLE>);
	/**
	 * @ingroup TupleCall
	 * \~English
	 * @brief Get tuple result type when function is invoked.
	 * @tparam FUNCTION Function That would be called.
	 * @tparam TUPLE Tuple that should be insert.
	 *
	 * \~German
	 * @brief Get tuple result type when function is invoked.
	 * @tparam FUNCTION Function That would be called.
	 * @tparam TUPLE Tuple that should be insert.
	 */
	template<class FUNCTION, ::m1e4::utility::TupleConcept TUPLE>
	requires ::m1e4::utility::TupleCallable<FUNCTION, TUPLE>
	using tupleCallResult =
		typename ::m1e4::utility::internal::TupleWorker<::std::remove_cvref_t<TUPLE>>::getResultType<FUNCTION>;

	/**
	 * @ingroup TupleCall
	 * \~English
	 * @brief Calls a function with the tuple as r-reference or shouldn't that be possible with the tuple unpacked.
	 * @tparam TUPLE The tuple type that is used.
	 * @tparam FUNCTION The function type that should be called.
	 * @return Return value of that function.
	 *
	 * \~Germany
	 * @brief Ruft eine Funktion mit dem Tupel als R-Referenz auf oder, wenn dies nicht möglich ist, entpackte R-Referenzen.
	 * @tparam TUPLE Der Tupeltyp, der verwendet wird.
	 * @tparam FUNCTION Die Funktion, die damit aufgerufen werden soll.
	 * @return Die Rückgabe der Funktion.
	 */
	template<::m1e4::utility::TupleConcept TUPLE, ::m1e4::utility::TupleCallable<TUPLE> FUNCTION>
	inline ::m1e4::utility::tupleCallResult<FUNCTION, TUPLE> tupleCall(FUNCTION&& func, TUPLE&& tuple) {
		using TW = ::m1e4::utility::internal::TupleWorker<::std::remove_cvref_t<TUPLE>>;
		return TW::callFunc(::std::forward<FUNCTION>(func), ::std::forward<TUPLE>(tuple));
	}
} // namespace m1e4::utility

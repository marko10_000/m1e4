// Lizenziert unter der EUPL
// SPDX-License-Identifier: EUPL-1.2
#include <m1e4/utility/tupleCall.hpp>

#include <catch2/catch.hpp>

TEST_CASE("tupleCall", "[utility][tupleCall]") {
	using ::m1e4::utility::tupleCall;
	{
		auto f1 = [](int v1, int v2, int v3) -> int { return v1 + v2 + v3; };
		auto f2 = [](auto&& t) -> int { return ::std::get<0>(t) + ::std::get<1>(t) + ::std::get<2>(t); };
		auto f3 = [](int& v1, int& v2, int& v3) -> int { return v1 + v2 + v3; };
		auto t  = ::std::make_tuple(1, 2, 3);
		REQUIRE(tupleCall(f1, t) == 6);
		REQUIRE(tupleCall(f2, t) == 6);
		STATIC_REQUIRE_FALSE(::m1e4::utility::isTupleCallable<decltype(f3), decltype(t)>);
	}
}

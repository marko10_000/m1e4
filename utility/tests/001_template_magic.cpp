// Lizenziert unter der EUPL
// SPDX-License-Identifier: EUPL-1.2
#include <m1e4/utility/templateMagic.hpp>

#include <catch2/catch.hpp>
#include <type_traits>

//
// TChar
//
TEST_CASE("TChar", "[utility][templateMagic]") {
	using ::m1e4::utility::TChar;

	STATIC_REQUIRE(TChar<'a'>::value == 'a');
	STATIC_REQUIRE(TChar<0>::value == 0);
}

//
// TInteger
//
TEST_CASE("TInteger", "[utility][templateMagic]") {
	using ::m1e4::utility::TInteger;

	STATIC_REQUIRE(TInteger<-10>::value == -10);
	STATIC_REQUIRE(TInteger<123456>::value == 123456);
}

//
// TTypes
//
TEST_CASE("TTypes", "[utility][templateMagic]") {
	using ::m1e4::utility::TTypes;

	{
		using T = TTypes<void, char, int, float>;

		STATIC_REQUIRE(T::isUnique);
		STATIC_REQUIRE(T::contains<void>);
		STATIC_REQUIRE(T::contains<char>);
		STATIC_REQUIRE(T::contains<int>);
		STATIC_REQUIRE(T::contains<float>);
		STATIC_REQUIRE_FALSE(T::contains<double>);
	}

	{
		using T = TTypes<void, char, int, int>;

		STATIC_REQUIRE_FALSE(T::isUnique);
		STATIC_REQUIRE(T::contains<void>);
		STATIC_REQUIRE(T::contains<char>);
		STATIC_REQUIRE(T::contains<int>);
		STATIC_REQUIRE_FALSE(T::contains<float>);
		STATIC_REQUIRE_FALSE(T::contains<double>);
	}

	{
		using T = TTypes<void, char, int, void>;

		STATIC_REQUIRE_FALSE(T::isUnique);
		STATIC_REQUIRE(T::contains<void>);
		STATIC_REQUIRE(T::contains<char>);
		STATIC_REQUIRE(T::contains<int>);
		STATIC_REQUIRE_FALSE(T::contains<float>);
		STATIC_REQUIRE_FALSE(T::contains<double>);
	}

	{
		using T = TTypes<void, float, int, float>;

		STATIC_REQUIRE_FALSE(T::isUnique);
		STATIC_REQUIRE(T::contains<void>);
		STATIC_REQUIRE_FALSE(T::contains<char>);
		STATIC_REQUIRE(T::contains<int>);
		STATIC_REQUIRE(T::contains<float>);
		STATIC_REQUIRE_FALSE(T::contains<double>);
	}
}

//
// TIntegers
//
TEST_CASE("TIntegers", "[utility][templateMagic]") {
	using ::m1e4::utility::TInteger;
	using ::m1e4::utility::TIntegers;
	using ::m1e4::utility::TTypes;

	{
		using T  = TIntegers<>;
		using TT = TTypes<>;

		STATIC_REQUIRE_FALSE(T::contains<0>);
		STATIC_REQUIRE(T::isUnique);
		STATIC_REQUIRE(::std::is_same_v<T::asTTypes, TT>);
	}

	{
		using T  = TIntegers<-2, -1, 0, 1, 2>;
		using TT = TTypes<TInteger<-2>, TInteger<-1>, TInteger<0>, TInteger<1>, TInteger<2>>;

		STATIC_REQUIRE_FALSE(T::contains<-3>);
		STATIC_REQUIRE(T::contains<-2>);
		STATIC_REQUIRE(T::contains<-1>);
		STATIC_REQUIRE(T::contains<0>);
		STATIC_REQUIRE(T::contains<1>);
		STATIC_REQUIRE(T::contains<2>);
		STATIC_REQUIRE_FALSE(T::contains<3>);
		STATIC_REQUIRE(T::isUnique);
		STATIC_REQUIRE(::std::is_same_v<T::asTTypes, TT>);
	}

	{
		using T  = TIntegers<1, 2, 1>;
		using TT = TTypes<TInteger<1>, TInteger<2>, TInteger<1>>;

		STATIC_REQUIRE_FALSE(T::contains<0>);
		STATIC_REQUIRE(T::contains<1>);
		STATIC_REQUIRE(T::contains<2>);
		STATIC_REQUIRE_FALSE(T::contains<3>);
		STATIC_REQUIRE_FALSE(T::isUnique);
		STATIC_REQUIRE(::std::is_same_v<T::asTTypes, TT>);
	}

	{
		using T  = TIntegers<1, 2, 3, 2>;
		using TT = TTypes<TInteger<1>, TInteger<2>, TInteger<3>, TInteger<2>>;

		STATIC_REQUIRE_FALSE(T::contains<0>);
		STATIC_REQUIRE(T::contains<1>);
		STATIC_REQUIRE(T::contains<2>);
		STATIC_REQUIRE(T::contains<3>);
		STATIC_REQUIRE_FALSE(T::contains<4>);
		STATIC_REQUIRE_FALSE(T::isUnique);
		STATIC_REQUIRE(::std::is_same_v<T::asTTypes, TT>);
	}

	{
		using T  = TIntegers<1, 2, 3, 3>;
		using TT = TTypes<TInteger<1>, TInteger<2>, TInteger<3>, TInteger<3>>;

		STATIC_REQUIRE_FALSE(T::contains<0>);
		STATIC_REQUIRE(T::contains<1>);
		STATIC_REQUIRE(T::contains<2>);
		STATIC_REQUIRE(T::contains<3>);
		STATIC_REQUIRE_FALSE(T::contains<4>);
		STATIC_REQUIRE_FALSE(T::isUnique);
		STATIC_REQUIRE(::std::is_same_v<T::asTTypes, TT>);
	}

	{
		using T  = TIntegers<1, 2, 3, 1>;
		using TT = TTypes<TInteger<1>, TInteger<2>, TInteger<3>, TInteger<1>>;

		STATIC_REQUIRE_FALSE(T::contains<0>);
		STATIC_REQUIRE(T::contains<1>);
		STATIC_REQUIRE(T::contains<2>);
		STATIC_REQUIRE(T::contains<3>);
		STATIC_REQUIRE_FALSE(T::contains<4>);
		STATIC_REQUIRE_FALSE(T::isUnique);
		STATIC_REQUIRE(::std::is_same_v<T::asTTypes, TT>);
	}
}

//
// isTTypes
//
TEST_CASE("isTTypes", "[utility][templateMagic]") {
	using ::m1e4::utility::isTTypes;
	using ::m1e4::utility::TTypes;

	STATIC_REQUIRE(isTTypes<TTypes<>>);
	STATIC_REQUIRE(isTTypes<TTypes<int>>);
	STATIC_REQUIRE(isTTypes<TTypes<void, void>>);
	STATIC_REQUIRE(isTTypes<TTypes<int, void, void>>);
	STATIC_REQUIRE_FALSE(isTTypes<void>);
	STATIC_REQUIRE_FALSE(isTTypes<int>);
	STATIC_REQUIRE_FALSE(isTTypes<float>);
}

//
// TIntegersBoundary
//
TEST_CASE("TIntegersBoundary", "[utility][templateMagic]") {
	using ::m1e4::utility::TIntegers;
	using ::m1e4::utility::TIntegersBoundary;

	{
		using T  = TIntegersBoundary<-10, -7>;
		using TT = TIntegers<-10, -9, -8, -7>;

		STATIC_REQUIRE(::std::is_same_v<T, TT>);
	}

	{
		using T  = TIntegersBoundary<0, 0>;
		using TT = TIntegers<0>;

		STATIC_REQUIRE(::std::is_same_v<T, TT>);
	}

	{
		using T  = TIntegersBoundary<-10, -11>;
		using TT = TIntegers<>;

		STATIC_REQUIRE(::std::is_same_v<T, TT>);
	}

	{
		using T  = TIntegersBoundary<0, -1>;
		using TT = TIntegers<>;

		STATIC_REQUIRE(::std::is_same_v<T, TT>);
	}
}

//
// TIntegersFromSize
//
TEST_CASE("TIntegersFromSize", "[utility][templateMagic]") {
	using ::m1e4::utility::TIntegers;
	using ::m1e4::utility::TIntegersFromSize;

	STATIC_REQUIRE(::std::is_same_v<TIntegersFromSize<-2>, TIntegers<>>);
	STATIC_REQUIRE(::std::is_same_v<TIntegersFromSize<-1>, TIntegers<>>);
	STATIC_REQUIRE(::std::is_same_v<TIntegersFromSize<0>, TIntegers<>>);
	STATIC_REQUIRE(::std::is_same_v<TIntegersFromSize<1>, TIntegers<0>>);
	STATIC_REQUIRE(::std::is_same_v<TIntegersFromSize<2>, TIntegers<0, 1>>);
	STATIC_REQUIRE(::std::is_same_v<TIntegersFromSize<3>, TIntegers<0, 1, 2>>);
	STATIC_REQUIRE(::std::is_same_v<TIntegersFromSize<4>, TIntegers<0, 1, 2, 3>>);
}

//
// TString
//
TEST_CASE("TString", "[utility][templateMagic]") {
	REQUIRE(TSTRING("").size == 0);
	REQUIRE(TSTRING("").content.size() == 0);
	REQUIRE(TSTRING("").content == "");
	REQUIRE(TSTRING("ABC").size == 3);
	REQUIRE(TSTRING("ABC").content == "ABC");
	REQUIRE(TSTRING("ABC\x00GHI").size == 7);
	REQUIRE(TSTRING("ABC\x00GHI").content.size() == 7);
	REQUIRE(TSTRING("ABC\x00GHI").content == ::std::string_view("ABC\x00GHI", 7));
}

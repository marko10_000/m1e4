// Lizenziert unter der EUPL
// SPDX-License-Identifier: EUPL-1.2
#include <m1e4/utility/drbg.hpp>

#include <catch2/catch.hpp>

using ::m1e4::utility::DRBG;
using ::m1e4::utility::XorshiftPlus128;

static_assert(DRBG<XorshiftPlus128>);

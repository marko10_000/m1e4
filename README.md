# M1E4
Marko10_000s C++ Hilfsbibliothek.

## Lizenz
Das Projekt und sein Inhalt sind unter der [EUPL-1.2](https://ec.europa.eu/info/european-union-public-licence) lizenziert.
Eine Kopie der Lizenz kann in `LICENCE.md` gefunden werden.

Bitte verwende den folgenden Dateikopf:
```
// Lizenziert unter der EUPL
// SPDX-License-Identifier: EUPL-1.2
```

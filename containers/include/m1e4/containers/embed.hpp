// Lizenziert unter der EUPL
// SPDX-License-Identifier: EUPL-1.2
#pragma once
#include <concepts>
#include <m1e4/utility/sanitizer.hpp>
#include <utility>

namespace m1e4::containers {
	/**
	 * @ingroup Containers
	 * \~English
	 * @brief
	 * @tparam CONTENT Content type that should be embedded.
	 * @{
	 *
	 * \~German
	 * @brief Bettet
	 * @tparam CONTENT Inhaltstyp der eingebettet werden soll.
	 * @{
	 */
	template<class CONTENT>
	struct Embedded;
	template<::std::movable CONTENT>
	struct Embedded<CONTENT> final {
		private:
			/// \~English Content storage.
			/// \~German Speicherort des Inhalts.
			char _data[sizeof(CONTENT)] = {};

			/**
			 * \~English
			 * @brief Get internal access of the content pointer.
			 * @return Pointer to content.
			 * @{
			 *
			 * \~German
			 * @brief Erlaubt interner Zugriff auf den Inhalt.
			 * @return Zeiger zum Inhalt.
			 * @{
			 */
			CONTENT* _get() {
				return static_cast<CONTENT*>(static_cast<void*>(this->_data));
			}
			const CONTENT* _get() const {
				return static_cast<const CONTENT*>(static_cast<const void*>(this->_data));
			}
			/// @}

			// Größenüberprüfung
			static_assert(sizeof(char) == 1);
			static_assert(sizeof(_data) == sizeof(CONTENT));

		public:
			/**
			 * \~English
			 * @brief Pointer access operation.
			 * @return Access reference.
			 * @{
			 *
			 * \~German
			 * @brief Zeigerzugriff-Operator.
			 * @return Zugriffreferenz.
			 * @{
			 */
			CONTENT& operator->() {
				return this->get();
			}
			CONTENT& operator->() const {
				return this->get();
			}
			/// @}

			/**
			 * \~English
			 * @brief Initialize the content with the move constructor.
			 * @param content Contant that should be moved.
			 * @return Reference to the content.
			 *
			 * \~German
			 * @brief Initialisiere den Inhalt mit dem Move-Konstruktor.
			 * @param content Inhalt, der rein bewegt werden soll.
			 * @return Referenz zu dem erstellten Inhalt.
			 */
			CONTENT& create(CONTENT&& content) {
				CONTENT* result = this->_get();
				new(result) CONTENT(::std::move(content));
				return *result;
			}
			/**
			 * \~English
			 * @brief Destroys the content.
			 *
			 * \~German
			 * @brief Zerstört den Inhalt.
			 */
			void destroy() {
				this->_get()->~CONTENT();
				M1E4_MEM_MAKE_UNDEFINED(this->_get(), sizeof(CONTENT));
			}
			/**
			 * \~English
			 * @brief Get access of the content.
			 * @return Reference of the content.
			 * @{
			 *
			 * \~German
			 * @brief Erlaubt Zugriff auf den Inhalt.
			 * @return Referenz zum Inhalt.
			 * @{
			 */
			CONTENT& get() {
				return *(this->_get());
			}
			const CONTENT& get() const {
				return *(this->_get());
			}
			/// @}
			/**
			 * \~English
			 * @brief Make a move from existing embedded by initialize own content.
			 * @param content Source that should be processed.
			 *
			 * \~German
			 * @brief Bewegt einen existierenden eingebetteten Wert, beidem der Inhalt neu initialisiert wird.
			 * @param content Inhalt, der eingefügt werden soll.
			 */
			void moveCreate(Embedded&& content) {
				new(this->_get()) CONTENT(::std::move(content->get()));
			};
			/**
			 * \~English
			 * @brief Make a move from existing embedded by setting new value of existing content.
			 * @param content Source that should be processed.
			 *
			 * \~German
			 * @brief Bewegt einen existierenden eingebetteten Wert, beidem existierender Inhalt gesetzt wird.
			 * @param content Inhalt, der eingefügt werden soll.
			 */
			void moveExisting(Embedded&& content) {
				(*(this->_get())) = ::std::move(content->get());
			}
	};
	/// @}
	/**
	 * @ingroup Containers
	 * \~English
	 * @brief Embedding void.
	 *
	 * \~German
	 * @brief Nichts einbetten.
	 */
	template<>
	struct Embedded<void> final {
		public:
			/**
			 * \~English
			 * @brief Make a move from existing embedded by initialize own content.
			 * @param content Source that should be processed.
			 *
			 * \~German
			 * @brief Bewegt einen existierenden eingebetteten Wert, beidem der Inhalt neu initialisiert wird.
			 * @param content Inhalt, der eingefügt werden soll.
			 */
			void moveCreate([[maybe_unused]] Embedded&& content) {}
			/**
			 * \~English
			 * @brief Make a move from existing embedded by setting new value of existing content.
			 * @param content Source that should be processed.
			 *
			 * \~German
			 * @brief Bewegt einen existierenden eingebetteten Wert, beidem existierender Inhalt gesetzt wird.
			 * @param content Inhalt, der eingefügt werden soll.
			 */
			void moveExisting([[maybe_unused]] Embedded&& content) {}
	};
} // namespace m1e4::containers

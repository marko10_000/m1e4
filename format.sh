#! /bin/sh

cd "$(dirname "$0")" &&
find . -type f '-(' -iname '*.h' -or -iname '*.c' -or -iname '*.hpp' -or -iname '*.cpp' '-)' | sort -f | \
	exec xargs -I{} -P "$(expr "$(nproc)" '*' 5 / 4)" sh -c 'echo "format: $0 " && clang-format -i "$0"' {}
